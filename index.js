const crypto = require('crypto')
const http = require('http');
const net = require('net');
const nstatic = require('node-static');

const RFC_WS_KEY = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
const file = new (nstatic.Server)(__dirname);

const sockets = new Set();

sockets.broadcast = function (data) {
    for (let socket of this) {
        socket.write(data);
    }
}

function genAcceptKey(key) {
    let shaSum = crypto.createHash('sha1');
    shaSum.update(key + RFC_WS_KEY);
    key = shaSum.digest('base64');
    return key;
}

function acceptUpgradeResponse(key) {
    return 'HTTP/1.1 101 Switching Protocols\r\n' +
        'Upgrade: websocket\r\n' +
        'Connection: Upgrade, Keep-Alive\r\n' +
        'Keep-Alive: 300\r\n' +
        'Sec-WebSocket-Accept: ' + key + '\r\n\r\n';
}

http.createServer(function (req, res) {
    file.serve(req, res);
}).listen(3030);

function decodeMessage(data) {
    let length = (data[1] & 127).valueOf();
    let decoded = data.subarray(6, length + 6);
    for (let i = 0; i < length; i++) {
        decoded[i] ^= data[2 + (i % 4)];
    }
    return decoded;
}

function encodeMessage(message) {
    let encoded = new Buffer.from("\x00\x00" + message);
    encoded[0] |= 128 | 1;
    encoded[1] |= message.length;
    return encoded;
}

const wsServer = net.createServer(function(socket) {
    console.log('Client connected');
    sockets.add(socket);

    socket.on('data', (data) => {
        if (data.includes("Upgrade: websocket")) {
            console.log("Upgrade requested");
            let wsk = data.toString().match("(?:Sec-WebSocket-Key: )(.+)[\r\n]")[1];
            let key = genAcceptKey(wsk);
            console.log("Key from client: " + wsk + "\nResponse key: " + genAcceptKey(wsk));
            socket.write(acceptUpgradeResponse((key)));
        } else if (data.length > 0) {
            let msgLength = (data[1] & 127).valueOf();
            console.log("Got message. FIN=%s, Masked=%s, Length=%s", data[0] & 128 ? "Y" : "N", data[1] & 128 ? "Y" : "N", msgLength);
            let message = decodeMessage(data);
            console.log("From client: " + message.toString());
            let answer = encodeMessage("Server says hi to all!");
            console.log("To clients: " + answer.subarray(2).toString());
            sockets.broadcast(answer);
        } else {
            console.log("Got empty message");
        }
    });

    socket.on('end', () => {
        console.log('Client disconnected');
        sockets.delete(socket);
    });

});

wsServer.on('error', (error) => {
    console.error('Error: ', error);
});

wsServer.listen(3033, () => {
    console.log('WebSocket server listening on port 3033');
});

